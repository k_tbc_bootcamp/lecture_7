package com.lkakulia.lecture_7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    //declare arrays
    lateinit var imageViewArray: Array<ImageView>
    lateinit var mutableImageArray: MutableList<Int>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    //init function
    private fun init() {

        //initialize arrays
        imageViewArray = arrayOf(imageView1, imageView2, imageView3, imageView4)
        mutableImageArray = mutableListOf<Int>(R.drawable.one, R.drawable.two,
            R.drawable.three, R.drawable.four, R.drawable.five, R.drawable.six, R.drawable.seven,
            R.drawable.eight, R.drawable.nine, R.drawable.ten)

        //set onClick listeners
        for (imageView in imageViewArray)
            imageView.setOnClickListener(this)
    }

    //generate a random view in imageViewArray
    private fun randomView() = imageViewArray[(imageViewArray.indices).random()]

    //generate a random image from mutableImageArray
    private fun randomImage() = mutableImageArray[(mutableImageArray.indices).random()]

    //change to the random image on the random view
    private fun changeImage(view: ImageView, image: Int) = view.setImageResource(image)

    //remove the random image to maintain uniqueness
    private fun removeImage(image: Int) = mutableImageArray.remove(image)

    override fun onClick(v: View?) {
        if (mutableImageArray.size > 0) {
            //get random view and image
            val randomView = randomView()
            val randomImage = randomImage()

            //change the image
            changeImage(randomView, randomImage)

            //remove the image from the array
            removeImage(randomImage)
        }

        else {
            Toast.makeText(this, "No more images to show.", Toast.LENGTH_LONG).show()
        }
    }
}
